#!/usr/bin/env bash

if [ $# -ne 6 ]
then
	return -1
fi

echo "uds-$1 slots=1" > hostfile_1.txt

echo "uds-$1 slots=2" > hostfile_2.txt

echo "uds-$1 slots=3" > hostfile_3.txt

echo "uds-$1 slots=4" > hostfile_4.txt

echo "uds-$1 slots=1" > hostfile_5.txt
echo "uds-$2 slots=1" >> hostfile_5.txt
echo "uds-$3 slots=1" >> hostfile_5.txt
echo "uds-$4 slots=1" >> hostfile_5.txt
echo "uds-$5 slots=1" >> hostfile_5.txt

echo "uds-$1 slots=3" > hostfile_6.txt
echo "uds-$2 slots=3" >> hostfile_6.txt

echo "uds-$1 slots=4" > hostfile_8.txt
echo "uds-$2 slots=4" >> hostfile_8.txt

echo "uds-$1 slots=2" > hostfile_10.txt
echo "uds-$2 slots=2" >> hostfile_10.txt
echo "uds-$3 slots=2" >> hostfile_10.txt
echo "uds-$4 slots=2" >> hostfile_10.txt
echo "uds-$5 slots=2" >> hostfile_10.txt

echo "uds-$1 slots=4" > hostfile_12.txt
echo "uds-$2 slots=4" >> hostfile_12.txt
echo "uds-$3 slots=4" >> hostfile_12.txt

echo "uds-$1 slots=3" > hostfile_15.txt
echo "uds-$2 slots=3" >> hostfile_15.txt
echo "uds-$3 slots=3" >> hostfile_15.txt
echo "uds-$4 slots=3" >> hostfile_15.txt
echo "uds-$5 slots=3" >> hostfile_15.txt

echo "uds-$1 slots=4" > hostfile_16.txt
echo "uds-$2 slots=4" >> hostfile_16.txt
echo "uds-$3 slots=4" >> hostfile_16.txt
echo "uds-$4 slots=4" >> hostfile_16.txt

echo "uds-$1 slots=4" > hostfile_20.txt
echo "uds-$2 slots=4" >> hostfile_20.txt
echo "uds-$3 slots=4" >> hostfile_20.txt
echo "uds-$4 slots=4" >> hostfile_20.txt
echo "uds-$5 slots=4" >> hostfile_20.txt

echo "uds-$1 slots=4" > hostfile_24.txt
echo "uds-$2 slots=4" >> hostfile_24.txt
echo "uds-$3 slots=4" >> hostfile_24.txt
echo "uds-$4 slots=4" >> hostfile_24.txt
echo "uds-$5 slots=4" >> hostfile_24.txt
echo "uds-$6 slots=4" >> hostfile_24.txt
