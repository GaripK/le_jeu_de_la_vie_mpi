// Jeu de la vie avec sauvegarde de quelques itérations
// compiler avec gcc -O3 -march=native (et -fopenmp si OpenMP souhaité)

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <mpi.h>

// hauteur et largeur de la matrice
#define HM 1200
#define LM 800

// nombre total d'itérations
#define ITER 10001
// multiple d'itérations à sauvegarder
#define SAUV 1000

#define DIFFTEMPS(a,b) \
(((b).tv_sec - (a).tv_sec) + ((b).tv_usec - (a).tv_usec)/1000000.)

/* tableau de cellules */
typedef char Tab[HM][LM];

// initialisation du tableau de cellules
void init(Tab);

// calcule une nouveau tableau de cellules à partir de l'ancien
// - paramètres : ancien, nouveau
void calcnouv(Tab, Tab, int, int);

// variables globales : pas de débordement de pile
Tab t1, t2;
Tab tsauvegarde[1+ITER/SAUV];

int main(int argc, char **argv)
{
  struct timeval tv_init, tv_beg, tv_end, tv_save;
  MPI_Init(&argc,&argv);

  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  gettimeofday( &tv_init, NULL);
  
  if(rank==0)
    init(t1);
  
  gettimeofday( &tv_beg, NULL);

  MPI_Scatter(&t1,
	     (HM*LM)/size,
	     MPI_CHAR,
	     &t1[1],
	     (HM*LM)/size,
	     MPI_CHAR,
	     0,
	     MPI_COMM_WORLD);

  for(int i=0 ; i<ITER ; i++)
  {
    if( i%2 == 0)
      calcnouv(t1, t2, rank, size);
    else
      calcnouv(t2, t1, rank, size);
    if(i%SAUV == 0)
    {
      // if(rank==0)
      // printf("sauvegarde (%d)\n", i);
      // copie t1 dans tsauvegarde[i/SAUV]
      for(int x=0 ; x<(HM/size) ; x++)
        for(int y=0 ; y<LM ; y++)
          tsauvegarde[i/SAUV][x][y] = t1[x+1][y];
    }
  }
  
  for(int i=0 ; i<ITER ; i+=SAUV)
    MPI_Gather(&tsauvegarde[i/SAUV],
	      (HM*LM)/size,
	      MPI_CHAR,
	      &tsauvegarde[i/SAUV],
	      (HM*LM)/size,
	      MPI_CHAR,
	      0,
	      MPI_COMM_WORLD);
  
  gettimeofday( &tv_end, NULL);
  
  if(rank==0)
  {
    FILE *f = fopen(argv[1], "w");
    for(int i=0 ; i<ITER ; i+=SAUV)
    {
      fprintf(f, "------------------ sauvegarde %d ------------------\n", i);
      for(int x=0 ; x<HM ; x++)
      {
        for(int y=0 ; y<LM ; y++)
          fprintf(f, tsauvegarde[i/SAUV][x][y]?"*":" ");
        fprintf(f, "\n");
      }
    }
    fclose(f);
    gettimeofday( &tv_save, NULL);

    // printf("init %lf\n", DIFFTEMPS(tv_init, tv_beg));
    printf("%lf\n", DIFFTEMPS(tv_beg, tv_end));
    // printf("sauvegarde %lf\n", DIFFTEMPS(tv_end, tv_save));
  }
  MPI_Finalize();
  return( 0 );
}

void init(Tab t)
{
  srand(time(0));
  int j;
  for(int i=0 ; i<HM ; i++)
  {
    for(j=0 ; j<LM ; j++ )
    {
      // t[i][j] = rand()%2;
      // t[i][j] = ((i+j)%3==0)?1:0;
      // t[i][j] = (i==0||j==0||i==HM-1||j==LM-1)?0:1;
      t[i][j] = 0;
    }
  }
    t[(8)][11] = 1;

    t[(9)][12] = 1;

    t[(10)][10] = 1;
    t[(10)][11] = 1;
    t[(10)][12] = 1;

    t[(54)][51] = 1;
    t[(54)][52] = 1;

    t[(55)][50] = 1;
    t[(55)][53] = 1;

    t[(56)][50] = 1;
    t[(56)][51] = 1;
    t[(56)][52] = 1;
}

int nbvois(Tab t, int i, int j)
{
  int n=0;
  if( i>0 )
  {  /* i-1 */
    if( j>0 )
      if( t[i-1][j-1] )
        n++;
    if( t[i-1][j] )
        n++;
    if( j<LM-1 )
      if( t[i-1][j+1] )
        n++;
  }
  if( j>0 )
    if( t[i][j-1] )
      n++;
  if( j<LM-1 )
    if( t[i][j+1] )
      n++;
  if( i<HM-1 )
  {  /* i+1 */
    if( j>0 )
      if( t[i+1][j-1] )
        n++;
    if( t[i+1][j] )
        n++;
    if( j<LM-1 )
      if( t[i+1][j+1] )
        n++;
  }
  return( n );
}
/*
int nbvois2(char ligne_en_bas[LM], char ligne_milieu[LM], char ligne_en_haut[LM], int j)
{
  int debut=j-1;
  int fin=j+1;
  if(debut<0)
    debut=j;
  if(fin==LM)
    fin=j;
  int n=0;
  for(int i=debut ; i< (fin+1) ; i++)
  {
    if(ligne_en_bas[i])
      n++;
    if(i!=j)
      if(ligne_milieu[i])
        n++;
    if(ligne_en_haut[i])
      n++;
  }
  return n;
}*/

void calcnouv(Tab t, Tab n, int rank, int size)
{
  int j;
  int debut=0;
  int fin=0;
  MPI_Request r1, r2, r3, r4;
  if(rank<(size-1))
  {
    fin=1;
    MPI_Issend(&t[(HM/size)],
              LM,
              MPI_CHAR,
              rank+1,
              2,
              MPI_COMM_WORLD,
	      &r1);
    MPI_Irecv(&t[((HM/size)+1)],
              LM,
              MPI_CHAR,
              rank+1,
              1,
              MPI_COMM_WORLD,
	      &r3);
  }
  if(rank>0)
  {
    debut=1;
    MPI_Issend(&t[1],
              LM,
              MPI_CHAR,
              rank-1,
              1,
              MPI_COMM_WORLD,
              &r2);
    MPI_Irecv(&t[0],
              LM,
              MPI_CHAR,
              rank-1,
              2,
              MPI_COMM_WORLD,
	      &r4);
  }


  for(int i=(debut+1) ; i<((HM/size)+1)-fin ; i++)
  {
    for(j=0 ; j<LM ; j++)
    {
      int v = nbvois(t, i, j);
      if(v==3)
        n[i][j] = 1;
      else if(v==2)
        n[i][j] = t[i][j];
      else
        n[i][j] = 0;
    }
  }
  if(rank<(size-1))
  {
    MPI_Wait(&r3,MPI_STATUS_IGNORE);
    if(rank!=0)
    MPI_Wait(&r1,MPI_STATUS_IGNORE);
    for(j=0 ; j<LM ; j++)
    {
      int v = nbvois(t, (HM/size), j);
      if(v==3)
        n[(HM/size)][j] = 1;
      else if(v==2)
        n[(HM/size)][j] = t[(HM/size)][j];
      else
        n[(HM/size)][j] = 0;
    }
  }
  if(rank>0)
  {
    MPI_Wait(&r4,MPI_STATUS_IGNORE);
    if(rank!=size-1)
    MPI_Wait(&r2,MPI_STATUS_IGNORE);
    for(j=0 ; j<LM ; j++)
    {
      int v = nbvois(t, 1, j);
      if(v==3)
        n[1][j] = 1;
      else if(v==2)
        n[1][j] = t[1][j];
      else
        n[1][j] = 0;
    }
  }
}
