#!/usr/bin/env bash

if [ $# -ne 2 ]
then
	exit -1
fi

`eval rm a.out`
`eval gcc -fopenmp -O3 -march=native -Wall -Werror -Wextra jedelvie.c && ./a.out`

`eval rm a.out`
`eval mpicc -O3 -march=native -Wall -Werror -Wextra jeudelavie.c`

echo "nbpc nbproc/pc nbproc temps" > $1
echo "nbpc nbproc/pc nbproc moyenne_temps" > $2

pc=(1 1 1 1 5 2 2 5 3 5 4 5 6)

procpc=(1 2 3 4 1 3 4 2 4 3 4 4 4)

bouclei=(1 2 3 4 5 6 8 10 12 15 16 20 24)
for i in ${!bouclei[*]}
do
	for j in {0..9}
	do
		mesures[$j]=`eval mpirun -hostfile hostfile_${bouclei[$i]}.txt -tune tune.txt -n ${bouclei[$i]} ./a.out jdlv_mpi_${bouclei[$i]}.out`
		cmp jdlv.out jdlv_mpi_${bouclei[$i]}.out
		if [ $? -ne 0 ]
		then
			rm jdlv.out jdlv_mpi_${bouclei[$i]}.out
			exit -2
		fi
		rm jdlv_mpi_${bouclei[$i]}.out
		echo "${pc[$i]} ${procpc[$i]} ${bouclei[$i]} ${mesures[$j]}" >> $1
		if [ $j -ne 0 ]
		then
			mesures[0]=`echo "scale=10 ; ${mesures[0]} +  ${mesures[$j]}" | bc`
		fi
	done
	mesure=`echo "scale=10 ; ${mesures[0]} / 10.0" | bc`
	echo "${pc[$i]} ${procpc[$i]} ${bouclei[$i]} $mesure" >> $2
done
`eval rm a.out`
rm jdlv.out
exit 0
