#!/usr/bin/env bash

if [ $# -ne 2 ]
then
        return -1
fi

`eval gcc -fopenmp -O3 -march=native -Wall -Wextra $1 && ./a.out && mpicc -O3 -march=native -Wall -Wextra $2 && mpirun -hostfile hostfile_16.txt -tune tune.txt -n 16 ./a.out jdlv_mpi.out`

cmp jdlv.out jdlv_mpi.out

