#!/usr/bin/env bash

if [ $# -ne 5 ]
then
        return -1
fi

sed -n $(expr $3 - 1202)p $1 | cut -c$4-$5
sed -n $(expr $3 - 1201)p $1 | cut -c$4-$5
sed -n $(expr $3 - 1200)p $1 | cut -c$4-$5
echo "------------------------------------------"
sed -n $(expr $3 - 1)p $1 | cut -c$4-$5
sed -n $3p $1 | cut -c$4-$5
sed -n $(expr $3 + 1)p $1 | cut -c$4-$5
echo "------------------------------------------"
sed -n $(expr $3 + 1200)p $1 | cut -c$4-$5
sed -n $(expr $3 + 1201)p $1 | cut -c$4-$5
sed -n $(expr $3 + 1202)p $1 | cut -c$4-$5
echo "------------------------------------------"
echo ""
echo "------------------------------------------"
sed -n $(expr $3 - 1202)p $2 | cut -c$4-$5
sed -n $(expr $3 - 1201)p $2 | cut -c$4-$5
sed -n $(expr $3 - 1200)p $2 | cut -c$4-$5
echo "------------------------------------------"
sed -n $(expr $3 - 1)p $2 | cut -c$4-$5
sed -n $3p $2 | cut -c$4-$5
sed -n $(expr $3 + 1)p $2 | cut -c$4-$5
echo "------------------------------------------"
sed -n $(expr $3 + 1200)p $2 | cut -c$4-$5
sed -n $(expr $3 + 1201)p $2 | cut -c$4-$5
sed -n $(expr $3 + 1202)p $2 | cut -c$4-$5
echo "------------------------------------------"
